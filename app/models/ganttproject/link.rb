module Ganttproject
  class Link < ActiveRecord::Base
    belongs_to :project

    validates_presence_of :project_id
    validates_presence_of :target
    validates_presence_of :source

    def from_params(params, session, id)
      self.source = params["#{id}_source"].to_i
      self.target = params["#{id}_target"].to_i
      self.gtype = params["#{id}_type"].to_i
      self.project_id=params[:id]
    end

  end
end
