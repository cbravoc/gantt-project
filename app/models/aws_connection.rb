class AwsConnection

  def initialize
    Aws.use_bundled_cert!
    Aws.config.update({ region: AWS_CONFIG["s3"]["region"], 
                        credentials: Aws::Credentials.new(AWS_CONFIG["s3"]["access_key"],AWS_CONFIG["s3"]["secret_access_key"])
    })
  end

  def s3_bucket(bucket_name=AWS_CONFIG["s3"]["bucket_name"])
    Aws::S3::Resource.new.bucket(bucket_name)
  end

  def s3_client(bucket_name=AWS_CONFIG["s3"]["bucket_name"])
    
  end

  def upload_file_to_s3(filename,filepath,options={})
    options[:acl] ||= "public-read"
    object = s3_bucket.object(filename)
    object.upload_file(filepath, acl: options[:acl])
    object.public_url.gsub("https","http") rescue nil
  end


end
