class AddSectorIdToTasksAndProjects < ActiveRecord::Migration
  def change
    add_column :ganttproject_projects, :sector_id, :integer
    add_column :ganttproject_tasks, :sector_id, :integer
  end
end
